﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZarX.Tools;
using ZarX.Tools.File;

namespace System
{
    public static class FileInfoExtension
    {
        public static string GetMimeType(this FileInfo fileInfo)
        {
            return MimeTypeResolver.GetMimeType(Path.GetExtension(fileInfo.FullName));
        }
    }
}
