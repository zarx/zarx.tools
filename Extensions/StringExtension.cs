﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ZarX.Tools.RegularExpressions;

namespace System
{
    public static class StringExtension
    {
        #region ToNumber
        public static int ToInt(this string str, int defaultValue = 0)
        {
            try
            {
                return int.Parse(str);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static decimal ToDecimal(this string str, decimal defaultValue = 0)
        {
            try
            {
                return decimal.Parse(str);
            }
            catch
            {
                return defaultValue;
            }
        }
        #endregion

        public static bool IsNullOrEmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }

        public static string[] Split(this string str, string separator, StringSplitOptions options = StringSplitOptions.None)
        {
            return str.Split(new string[] { str }, StringSplitOptions.RemoveEmptyEntries);
        }

        public static List<string> BreakLine(this string text, int maxCharsInLine)
        {
            return ZarX.Tools.TextTools.BreakLine(text, maxCharsInLine);
        }

        #region Case manipulation
        /// <summary>
        /// Converts string to proper case
        /// ex: "this is an example" = "This Is An Example"
        /// </summary>
        /// <returns>Proper cased string</returns>
        public static string ToProperCase(this string str)
        {
            var sArr = str.Split(' ');
            var sList = new List<string>();

            foreach (var s in sArr)
            {
                StringBuilder sb = new StringBuilder(s.ToLower());
                sb[0] = Char.ToUpper(sb[0]);
                sList.Add(sb.ToString());
            }

            return String.Join(" ", sList);
        }

        /// <summary>
        /// Converts string to sentence case
        /// ex: "these sentences. are examples." = "These sentences. Are examples."
        /// </summary>
        /// <returns>Sentence cased string</returns>
        public static string ToSentenceCase(this string str)
        {
            var sArr = str.Split(". ");
            var sList = new List<string>();

            foreach (var s in sArr)
            {
                StringBuilder sb = new StringBuilder(s.ToLower().Trim());
                if (sb.Length > 0)
                {
                    sb[0] = Char.ToUpper(sb[0]);
                }
                sList.Add(sb.ToString());
            }

            return String.Join(". ", sList);
        }
        #endregion

        /// <summary>
        /// WildCard compare of string
        /// ex: *.txt or file_?.txt
        /// </summary>
        /// <param name="pattern">WildCard pattern</param>
        /// <returns>True/False</returns>
        public static bool Like(this string str, string pattern)
        {
            return new WildCard(pattern, RegexOptions.IgnoreCase | RegexOptions.Singleline).IsMatch(str);
        }

        public static T ParseEnum<T>(string value, T defaultValue) where T : struct, IConvertible
        {
            if (!typeof(T).IsEnum) throw new ArgumentException("T must be an enumerated type");
            if (string.IsNullOrEmpty(value)) return defaultValue;

            foreach (T item in Enum.GetValues(typeof(T)))
            {
                if (item.ToString().ToLower().Equals(value.Trim().ToLower())) return item;
            }
            return defaultValue;
        }
    }
}
