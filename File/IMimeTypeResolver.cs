﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZarX.Tools.File
{
    public interface IMimeTypeResolver
    {
        string Get(string extension);
    }
}
