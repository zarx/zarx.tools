﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZarX.Tools
{
    public class TextTools
    {
        // TODO: Comment and add word break support
        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="maxCharsInLine"></param>
        /// <param name="removeEmptyRow"></param>
        /// <returns></returns>
        public static List<string> BreakLine(string text, int maxCharsInLine, bool removeEmptyRows = true)
        {
            var lstStr = new List<string>();
            int charsInLine = 0;
            lstStr.Add("");
            for (int i = 0; i < text.Length; i++)
            {
                char c = text[i];
                if (/*char.IsWhiteSpace(c) || */charsInLine >= maxCharsInLine)
                {
                    lstStr.Add("");
                    charsInLine = 0;
                }
                //else
                //{
                    lstStr[lstStr.Count - 1] += c;
                    charsInLine++;
                //}
            }

            if (removeEmptyRows)
                lstStr.RemoveAll(s => String.IsNullOrWhiteSpace(s));

            return lstStr;
        }
    }
}
