﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ZarX.Tools.Window
{
    public class Window
    {
        public Process process;
        public IntPtr hWnd;
        public string Text
        {
            get
            {
                int length = User32.GetWindowTextLength(this.hWnd);
                StringBuilder sb = new StringBuilder(length + 1);
                User32.GetWindowText(this.hWnd, sb, length);

                return sb.ToString();
            }
        }
        private User32.Rect _rect;
        public User32.Rect Rect
        {
            get
            {
                User32.GetWindowRect(this.hWnd, ref _rect);
                return _rect;
            }
        }

        public int Width { get { return this.Rect.Right - this.Rect.Left; } }
        public int Height { get { return this.Rect.Bottom - this.Rect.Top; } }
        public int Left { get { return this.Rect.Left; } }
        public int Top { get { return this.Rect.Top; } }

        public bool Move(int? left = null, int? top = null, int? width = null, int? height = null)
        {
            if (!left.HasValue)
                left = this.Left;

            if (!top.HasValue)
                top = this.Top;

            if (!width.HasValue)
                width = this.Left;

            if (!height.HasValue)
                height = this.Height;

            return User32.MoveWindow(this.hWnd, left.Value, top.Value, width.Value, height.Value, true);
        }

        public Window(IntPtr handle, Process parentProcess)
        {
            this.hWnd = handle;
            this.process = parentProcess;
        }

        #region Static
        private static StringBuilder _apiResult = new StringBuilder(256); //256 Is max class name length.
        private static List<Window> _windowList;
        private static string _className;
        private static Process _currentProcess;

        public static List<Window> Find(string processName)
        {
            var list = new List<Window>();
            
            Process[] processList = Process.GetProcessesByName(processName);
            foreach (Process process in processList)
            {
                if (process.MainWindowHandle == IntPtr.Zero)
                    continue;

                list.Add(new Window(process.MainWindowHandle, process));
            }

            return list;
        }

        public static List<Window> Find(string className, string processName)
        {
            _className = className;
            _windowList = new List<Window>();

            Process[] processList = Process.GetProcessesByName(processName);
            foreach (Process process in processList)
            {
                if (process.MainWindowHandle == IntPtr.Zero)
                    continue;

                _currentProcess = process;
                foreach (ProcessThread thread in process.Threads)
                {
                    User32.EnumThreadWindows((uint)thread.Id, new User32.EnumThreadDelegate(EnumThreadCallback), IntPtr.Zero);
                }
            }

            return _windowList;
        }

        private static bool EnumThreadCallback(IntPtr hWnd, IntPtr lParam)
        {
            if (User32.GetClassName(hWnd, _apiResult, _apiResult.Capacity) != 0)
            {
                if (string.CompareOrdinal(_apiResult.ToString(), _className) == 0)
                {
                    _windowList.Add(new Window(hWnd, _currentProcess));
                }
            }
            return true;
        }
        #endregion
    }
}
