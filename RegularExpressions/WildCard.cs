﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ZarX.Tools.RegularExpressions
{
    public class WildCard : Regex
    {
        public static string WildcardToRegex(string pattern)
        {
            return "^" + Regex.Escape(pattern).
             Replace("\\*", ".*").
             Replace("\\?", ".") + "$";
        }

        public WildCard(string pattern)
            : base(WildcardToRegex(pattern))
        {

        }

        public WildCard(string pattern, RegexOptions options)
            : base(WildcardToRegex(pattern), options)
        {

        }

        public WildCard(string pattern, RegexOptions options, TimeSpan timeSpan)
            : base(WildcardToRegex(pattern), options, timeSpan)
        {

        }
    }
}
